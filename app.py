from flask import Flask
app = Flask(__name__)

count = 0


def get_hit_count():
    global count
    count += 1
    return count


@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
